package apc.mobprog.firebasedemo;

import android.util.Log;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by ultim on 8/12/2017.
 */

public final class DbHelper {
    private static final DbHelper INSTANCE = new DbHelper();
    private static final String TAG = "DbHelper";

    private FirebaseDatabase db;
    private DatabaseReference studentRef;
    private List<Student> studentList;


    private DbHelper() {
        db = FirebaseDatabase.getInstance();
        studentList = new ArrayList<Student>();
        studentRef = db.getReference("student");
        studentRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                studentList.add(dataSnapshot.getValue(Student.class));
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public static DbHelper getInstance() {
        return INSTANCE;
    }

    public void addStudent(Student s) {
        studentRef.push().setValue(s);
    }

    public List<Student> getStudentList() {
        return studentList;
    }

}
