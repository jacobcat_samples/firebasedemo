package apc.mobprog.firebasedemo;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


public class MyStudentRecyclerViewAdapter extends RecyclerView.Adapter<MyStudentRecyclerViewAdapter.ViewHolder> {

    private DbHelper db;

    public MyStudentRecyclerViewAdapter() {
        db = DbHelper.getInstance();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_student, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = db.getStudentList().get(position);
        holder.list_student_id.setText(holder.mItem.getStudentId());
        holder.list_last_name.setText(holder.mItem.getLastName());
        holder.list_first_name.setText(holder.mItem.getFirstName());
        holder.list_course.setText(holder.mItem.getCourse());
    }

    @Override
    public int getItemCount() {
        return db.getStudentList().size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView list_student_id;
        public final TextView list_last_name;
        public final TextView list_first_name;
        public final TextView list_course;
        public Student mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            list_student_id = (TextView) view.findViewById(R.id.list_student_id);
            list_last_name = (TextView) view.findViewById(R.id.list_last_name);
            list_first_name = (TextView) view.findViewById(R.id.list_first_name);
            list_course = (TextView) view.findViewById(R.id.list_course);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + list_student_id.getText() + "'";
        }
    }
}
