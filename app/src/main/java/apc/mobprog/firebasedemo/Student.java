package apc.mobprog.firebasedemo;

import java.io.Serializable;

/**
 * Created by ultim on 8/12/2017.
 */

public class Student implements Serializable {

    private String studentId;
    private String lastName;
    private String firstName;
    private String course;

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getCourse() {
        return course;
    }

    public void setCourse(String course) {
        this.course = course;
    }
}
