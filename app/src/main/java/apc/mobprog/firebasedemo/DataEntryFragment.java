package apc.mobprog.firebasedemo;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DataEntryFragment extends Fragment {

    private final String TAG = "FirebaseDemo";
    private EditText student_id;
    private EditText last_name;
    private EditText first_name;
    private EditText course;
    private Button add_button;
    private DbHelper db;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.activity_data_entry, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initializeComponents(view);
    }

    private void initializeComponents(View view) {
        student_id = (EditText)view.findViewById(R.id.student_id);
        last_name = (EditText)view.findViewById(R.id.last_name);
        first_name = (EditText)view.findViewById(R.id.first_name);
        course = (EditText)view.findViewById(R.id.course);
        add_button = (Button)view.findViewById(R.id.add_button);

        add_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickAddButton(v);
            }
        });

        db = DbHelper.getInstance();


    }

    public void clickAddButton(View view) {
        Student s = new Student();
        s.setStudentId(student_id.getText().toString());
        s.setLastName(last_name.getText().toString());
        s.setFirstName(first_name.getText().toString());
        s.setCourse(course.getText().toString());
        db.addStudent(s);
        clearTextFields();
        Toast.makeText(getActivity(), R.string.success_msg, Toast.LENGTH_SHORT).show();
    }

    private void clearTextFields() {
        student_id.setText("");
        last_name.setText("");
        first_name.setText("");
        course.setText("");
    }
}
